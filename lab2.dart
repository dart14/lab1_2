import 'package:lab2/lab2.dart' as lab2;
import 'package:test/test.dart';
import 'dart:io';

void main() 
{
  int count;
  // String input = "Kim kim go Hell hell";
  print(" Enter String Data: ");
  String? input = stdin.readLineSync();
  String str = input!.toLowerCase();
  final word = str.split(' ');
  for (int i = 0; i < word.length; i++) {
    count = 1;
    for (int j = i+1; j < word.length; j++) {
      if (word[i] == word[j]) {
        count = count + 1;
        word[j] = "0";
      }
    }
    if(word[i] != "0"){
      print(word[i] +" = " + count.toString());
    }
  }
}
